from setuptools import setup, find_packages

__version__ = '0.0.3'

setup(
    name='lildoggie',
    version=__version__,
    description='Simplify watchdog',
    packages=find_packages(exclude=['docs', 'tests*']),
    include_package_data=True,
    install_requires=[
        'watchdog',
        ],
    url='',
    license='BSD',
    classifiers=[
      'Development Status :: 3 - Alpha',
      'Intended Audience :: Developers',
      'Programming Language :: Python :: 3',
    ],
    keywords='',
    author='Matt Schmidt',
    author_email='matt@digitaleye.com'
)
