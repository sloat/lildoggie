import lildoggie
from lildoggie import pm


@lildoggie.watch_pattern('./scss/', patterns=['*.scss'])
def scss_handler(event):
    pm.run_wait('sassc', 'sassc -t compressed scss/main.scss css/main.css')

pm.run_long('tsc', 'tsc -w src/main.ts --outDir js/')

lildoggie.start()

try:
    lildoggie.run_forever()
except KeyboardInterrupt:
    pass

