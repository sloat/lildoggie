import lildoggie


@lildoggie.watch_pattern('./scss/', patterns=['*.scss'])
def scss_handler(event):
    print('scss: ', event)

@lildoggie.watch_pattern('./src/')
def src_handler(event):
    print('src: ', event)


lildoggie.start()

try:
    lildoggie.run_forever()
except KeyboardInterrupt:
    pass

