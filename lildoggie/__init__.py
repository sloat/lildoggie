from watchdog.observers import Observer
from watchdog.events import (
        PatternMatchingEventHandler,
        RegexMatchingEventHandler,
        )
import time

from .process import *


__all__ = [
        'watch_pattern',
        'watch_regex',
        'start',
        'stop',
        'run_forever',
        'pm',
        ]


class WithCallback:
    cb = None

    def on_any_event(self, event):
        self.cb(event)

class PMEHWithCallback(WithCallback, PatternMatchingEventHandler):
    pass

class RMEHWithCallback(WithCallback, RegexMatchingEventHandler):
    pass


obs = Observer()

def watch_pattern(path, recursive=True, patterns=None, ignore_patterns=None,
            ignore_directories=False, case_sensitive=False, events=None):
    def wrapper(fn):
        global obs

        handler = PMEHWithCallback(patterns=patterns,
                ignore_patterns=ignore_patterns,
                ignore_directories=ignore_directories,
                case_sensitive=case_sensitive)

        def _intercept(event):
            if events is None or event.event_type in events:
                fn(event)

        handler.cb = _intercept

        obs.schedule(handler, path, recursive=recursive)
    return wrapper

def watch_regex(path, recursive=True, regexes=['.*'], ignore_regexes=[],
            ignore_directories=False, case_sensitive=False, events=None):
    def wrapper(fn):
        global obs

        handler = RMEHWithCallback(regexes=regexes,
                ignore_regexes=ignore_regexes,
                ignore_directories=ignore_directories,
                case_sensitive=case_sensitive)

        def _intercept(event):
            if events is None or event.event_type in events:
                fn(event)

        handler.cb = fn

        obs.schedule(handler, path, recursive=recursive)
    return wrapper

def start():
    global obs
    obs.start()

def stop():
    global obs
    obs.stop()

def run_forever():
    global obs

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt as ex:
        obs.stop()
        raise ex


