from subprocess import Popen, PIPE
import shlex
import sys
import multiprocessing as mp
import time


__all__ = [
        'ProcessManager',
        'pm',
        ]

class ProcessManager:
    processes = {}

    def __del__(self):
        self.stop_all()


    def run_long(self, pname, cmd, iopipe=False):
        if pname in self.processes:
            p = self.processes[pname]
            p.kill()

        if not iopipe:
            stdin = None
            stdout = sys.stdout
            stderr = sys.stderr
        else:
            stdin = stdout = stderr = PIPE

        self.processes[pname] = Popen(shlex.split(cmd),
                stdin=stdin,
                stdout=stdout,
                stderr=stderr)

    def run_wait(self, pname, cmd, iopipe=False):
        if pname in self.processes:
            return

        if not iopipe:
            stdin = None
            stdout = sys.stdout
            stderr = sys.stderr
        else:
            stdin = stdout = stderr = PIPE

        self.processes[pname] = Popen(shlex.split(cmd),
                stdin=stdin,
                stdout=stdout,
                stderr=stderr)
        self.processes[pname].wait()

        del self.processes[pname]

    def run_fn(self, pname, fn, *args):
        if pname in self.processes:
            p = self.processes[pname]
            if p.is_alive():
                print(f'{pname} is already running')
                self.stop(pname)

        p = mp.Process(target=fn, args=args)
        self.processes[pname] = p

        p.start()


    def stop(self, pname):
        p = self.processes[pname]

        if isinstance(p, Popen):
            p.kill()
        elif isinstance(p, mp.Process):
            p.terminate()
            p.join()

        # time.sleep(0.1)

    def stop_all(self):
        for pname in self.processes:
            self.stop(pname)


pm = ProcessManager()
