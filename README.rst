Lildoggie is a wrapper for the watchdog library that should make it much simpler to use.

Also included is a process manager for spawning short or long-running commands 
(e.g. a build process or development server)

This will probably not be released on PyPI because I don't feel like writing unit tests or documentation.

pip install git+https://gitlab.com/sloat/lildoggie.git
